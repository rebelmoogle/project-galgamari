using UnityEngine;
using System.Collections;

public class DestructionHandler : MonoBehaviour {

	public ParticleSystem splosions;
	public GameObject asteroidBits;
	public int numberOfBits;

	public float startHealth = 5.0f;
	public float baseXplosionForce = 50.0f;
	public float baseXplosionRadius = 5.0f;

    public delegate void EventHandler(GameObject e);
    public event EventHandler DestructionImminentEvent;
	public delegate void LiveChangeHandler(GameObject e, float newLife);
	public event LiveChangeHandler LivesChange;

	ScoreHandler spacepoints;
	
	float currentHealth;
	public float CurrentHealth {
		get {return currentHealth;}
		//set {currentHealth = value;}
	}
	
	// Use this for initialization
	void Start () 
	{
		currentHealth = startHealth;
		GameObject scoreText = GameObject.Find("ScoreText");
		if(scoreText != null)
			spacepoints = scoreText.GetComponent<ScoreHandler>();
		else
			Debug.LogError("NO SCORE TEXT! THIS WILL EXPLODE!");


	
	}
	
	// Update is called once per frame
	void Update () 
	{
		// if out of health trigger event.
		if(currentHealth <= 0.0f)
		{
			if(this.tag == "Obstacle" || this.tag == "Enemy")
			{
				if(spacepoints != null)
					spacepoints.addScore(1);
				else
					Debug.LogError("Someone forgot the score handler again!");
			}

			//blast off parts
			this.BroadcastMessage("BlastIntoSpace", new Vector2(baseXplosionForce, baseXplosionRadius), SendMessageOptions.DontRequireReceiver);

			//TODO: trigger event for more complex actions (menu, restart, etc)
			// for now just explode the ship
			// replace gameObject with explosion prefab
			ParticleSystem splosionClone;
			splosionClone = (ParticleSystem)Instantiate(splosions, transform.position, transform.rotation);
			//GameObject asteroidClone;
			for(int i = 0; i < numberOfBits; i++)
			{
				asteroidBits = (GameObject)Instantiate(this.gameObject, transform.position + Random.onUnitSphere, transform.rotation);
				asteroidBits.transform.localScale = transform.localScale * 0.25f;
				asteroidBits.GetComponent<DestructionHandler>().numberOfBits = Mathf.FloorToInt(numberOfBits / 2); // this smaller bit will blow up into fewer smaller bits.
				asteroidBits.GetComponent<Rigidbody>().velocity = Vector3.zero;
				asteroidBits.GetComponent<Rigidbody>().AddExplosionForce(baseXplosionForce, transform.position, baseXplosionRadius); // TODO: explosionforce
				asteroidBits.tag = "Obstacle";
				asteroidBits.GetComponent<Rigidbody>().freezeRotation = false;
				asteroidBits.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
				if(Application.loadedLevelName == "PlanetSide")
				{
					asteroidBits.GetComponent<Rigidbody>().useGravity = true;
					asteroidBits.transform.position += Vector3.up * 10f;
				}
					
					
			}
			Destroy(gameObject);
		}

		//TODO: handle if no camera

	}

	void OnCollisionEnter(Collision collision)
	{
		// on hit: generate debris!
		string colliderTag = collision.collider.transform.root.gameObject.tag; // always use the tag of the root.
		if(gameObject.CompareTag(colliderTag) || collision.collider.transform.root == this.transform.root) //ignore if same parents
			return; //ignore if same tag/type
		else if(colliderTag == "Obstacle" || colliderTag == "Enemy" || colliderTag == "Bullet")
		{
			// TODO: make dependent on strength / speed / power / etc.
			currentHealth -= 1.0f;
			OnLivesChange();
		}

	}

	void OnDestroy()
	{
        if (DestructionImminentEvent != null)
            DestructionImminentEvent(this.gameObject);
	}

	void OnLivesChange()
	{
		if(LivesChange != null)
			LivesChange(this.gameObject, currentHealth);
	}
}
