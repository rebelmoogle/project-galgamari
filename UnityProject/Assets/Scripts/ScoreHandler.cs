﻿using UnityEngine;
using System.Collections;

public class ScoreHandler : MonoBehaviour {

	float multiplikator = 1.0f;
	float currentScore = 0;

	public float Multiplier {
		get { return multiplikator; }
		set { multiplikator = value; }
	}

	public int CurrentScore {
		get { return Mathf.FloorToInt(currentScore); }
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		// update score gui text
		this.gameObject.GetComponent<GUIText>().text = Mathf.FloorToInt(currentScore).ToString() + " Spacepoints!";
	}

	// increase score handler
	public void addScore(float amount)
	{
		currentScore += amount * multiplikator;
	}

}
