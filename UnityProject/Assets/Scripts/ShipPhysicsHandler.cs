﻿using UnityEngine;
using System.Collections;

//does not actually use physics, cause I hate physics systems.. (and it would be overkill. :D)

//TODO optimize rotations
public class ShipPhysicsHandler: MonoBehaviour 
{
	public float moveSpeed = 10.0f;
	public float accelerationSpeed = 100.0f;
	public float bankTurnSpeed = 5.0f;
	public float hardSpeedMultiplier = 1.5f;
	public float barrelRollSpeed = 1.0f;
	public float rollPeriod = 0.5f;
	public float minimumDistance = 1.0f;
	public float maxBoost = 10.0f;

	Quaternion bankUp = Quaternion.AngleAxis(5, Vector3.left);
	Quaternion bankDown = Quaternion.AngleAxis(-5, Vector3.left);

	Quaternion bankHardRight = Quaternion.AngleAxis(15, Vector3.up);
	Quaternion bankHardLeft = Quaternion.AngleAxis(-15, Vector3.up);

	Quaternion bankLeft =  Quaternion.AngleAxis(-15, Vector3.up) * Quaternion.AngleAxis(-5, Vector3.left) * Quaternion.AngleAxis(15, Vector3.forward);
	Quaternion bankRight = Quaternion.AngleAxis(15, Vector3.up) * Quaternion.AngleAxis(-5, Vector3.left) * Quaternion.AngleAxis(-15 , Vector3.forward);


	Quaternion releaseRotation;
	Quaternion defaultRotation;

	public float rotateBackSpeed = 5.0f;

	Quaternion rotateToFixed;
	Vector3 translateHorizontal;
	Vector3 translateVertical;

	bool isBanking = false;
	bool isHardTurn = false;
	float barrelRollTimer = 0.0f;
	float barrelRollAngle = 0.0f;

	float boostSpeed = 1.0f;
	//bool isBoosting = false;
	
	//TODO: get from level dimensions
	[Range (10,1000)]
	public float levelWidth = 10.0f;
	[Range (10,1000)]
	public float levelHeight = 10.0f;
	Vector3 centralPosition;

	// Use this for initialization
	void Start () 
	{
		if(this.transform.root.CompareTag("Player"))
		{
			ShipInputHandler thisInputHandler = this.gameObject.GetComponent<ShipInputHandler>();
			if(thisInputHandler != null)
			{
				thisInputHandler.MoveHorizontal += MoveHorizontalListener;
				thisInputHandler.MoveVertical += MoveVerticalListener;
				thisInputHandler.Boost += BoostListener;
				thisInputHandler.HardLeft += HardLeftListener;
				thisInputHandler.HardRight += HardRightListener;
				thisInputHandler.MovementRelease += HandleMovementRelease;
			}
		}

		defaultRotation = this.gameObject.transform.rotation;
		releaseRotation = defaultRotation;
		rotateToFixed = defaultRotation;

		translateHorizontal = Vector3.zero;
		translateVertical = Vector3.zero;
		centralPosition = transform.position;
    }

	// Update is called once per frame
	void Update () 
	{
		// rotate slowly towards set rotation/direction
		//this.gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, rotatingTo, 1.0f);

		//rotate /angle back to default position when bankLastFrame == false

		// update unrelated stuff
		// handle collision events (EXPLOSIOOONS (effects component?))
	}

	void FixedUpdate()
	{

		// translations
		RaycastHit whatsDownThere;
		if(Physics.Raycast(this.transform.position, Vector3.down, out whatsDownThere))
		{
			if(  (translateVertical.y < 0 && whatsDownThere.distance < minimumDistance) 
			     && (translateVertical + translateHorizontal).magnitude >= 0.1f)
		   	{
				translateVertical = Vector3.zero;
			}
				
		}

        this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + ((translateHorizontal + translateVertical)* moveSpeed + (Vector3.forward * accelerationSpeed * boostSpeed) ) * Time.fixedDeltaTime);


		if(boostSpeed > 1.1f)
			boostSpeed -= 5.0f * Time.fixedDeltaTime;
		else if(boostSpeed < 0.9f)
			boostSpeed += 5.0f * Time.fixedDeltaTime;

		//	rotations
		if(barrelRollTimer > 0.0f)
		{
			this.gameObject.transform.Rotate(Vector3.forward, barrelRollAngle * Time.fixedDeltaTime * barrelRollSpeed);
			barrelRollTimer -= Time.fixedDeltaTime;
		}
		else if(isBanking)
		{
			this.gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, rotateToFixed, Time.fixedDeltaTime * bankTurnSpeed);
		}
		else
		{
			this.gameObject.transform.rotation = Quaternion.Slerp(this.gameObject.transform.rotation, releaseRotation, Time.fixedDeltaTime * rotateBackSpeed);
		}
//
		//boostSpeed = 1.0f;
//		// move forward at constant speeeeeed
		//this.gameObject.rigidbody.AddForce(Vector3.forward * accelerationSpeed * Time.fixedDeltaTime, ForceMode.VelocityChange);
		//this.gameObject.rigidbody.velocity = 

	}

	// Move up / down
	void MoveVerticalListener (GameObject e, float strength)
	{
		/*Quaternion currentRotation = this.gameObject.transform.rotation;
		this.gameObject.transform.rotation = defaultRotation;*/
		
			//TODO: make it get faster slower (slerp to maxspeed)
		float distanceFromCenter = Camera.main.transform.position.y - transform.position.y;
		if(Mathf.Abs(distanceFromCenter) < levelHeight || Mathf.Sign(distanceFromCenter) == Mathf.Sign(strength))
			translateVertical = Vector3.up * strength;
		else
			translateVertical = Vector3.zero;
		
		//this.gameObject.transform.rotation = currentRotation;
		

		if(strength < 0.0f)
		{
			rotateToFixed = bankDown * releaseRotation;
		}
		else if(strength > 0.0f) //if right
		{
			rotateToFixed = bankUp * releaseRotation;
		}
		
		
		isBanking = true;
	}

	// move left/right
	void MoveHorizontalListener(GameObject e, float strength)
	{
		//TODO: make it get faster slower (slerp to maxspeed)
//		Quaternion currentRotation = this.gameObject.transform.rotation;
//		this.gameObject.transform.rotation = defaultRotation;

		float distanceFromCenter = Camera.main.transform.position.x - transform.position.x;
		if(Mathf.Abs(distanceFromCenter) < levelWidth || Mathf.Sign(distanceFromCenter) == Mathf.Sign(strength))
			translateHorizontal = Vector3.right * strength;
		else
			translateHorizontal = Vector3.zero;
//		this.gameObject.transform.rotation = currentRotation;


		if(isHardTurn)
		{
			if(strength < 0.0f)
			{
				rotateToFixed = bankHardLeft * releaseRotation;
			}
			else if(strength > 0.0f) //if right
			{
				rotateToFixed =  bankHardRight * releaseRotation;
			}

		}
		else
		{

			//if left
			if(strength < 0.0f)
			{
				// bank left/right (also turn the nose abit)
				rotateToFixed = bankLeft;
			}
			else if(strength > 0.0f) //if right
			{
				// bank left/right (also turn the nose abit)
				rotateToFixed = bankRight;
			}

		}

		isBanking = true;
	}

	void BoostListener (GameObject e, float strength)
	{
		//isBoosting = true;
		boostSpeed += strength;

		if(boostSpeed > maxBoost)
			boostSpeed = maxBoost;
		else if(boostSpeed < 0.0f)
			boostSpeed = 0.0f;

	}

	void HardLeftListener (GameObject e, ButtonStates buttonState)
	{
		// HARD LEFT
		if(buttonState ==ButtonStates.isDoublePress)
		{
			// do a barrel roll
			barrelRollAngle = 180.0f;
			barrelRollTimer = rollPeriod;
		}
		else if(buttonState == ButtonStates.isHeld)
		{
			releaseRotation  = Quaternion.AngleAxis(90, Vector3.forward);
			isHardTurn = true;
		}
		else if(buttonState == ButtonStates.isRelease)
		{
			releaseRotation = defaultRotation;
			isHardTurn = false;
		}



	}

	void HardRightListener (GameObject e, ButtonStates buttonState)
	{
		// HARD RIGHT
		if(buttonState == ButtonStates.isDoublePress)
		{
			barrelRollAngle = -180.0f;
			barrelRollTimer = rollPeriod;
		}
		else if(buttonState == ButtonStates.isHeld)
		{
			releaseRotation  = Quaternion.AngleAxis(-90, Vector3.forward);
			isHardTurn = true;
		}
		else if(buttonState == ButtonStates.isRelease)
		{
			releaseRotation = defaultRotation;
			isHardTurn = false;
		}
	}

	void HandleMovementRelease (GameObject e)
	{
		isBanking = false;
		translateHorizontal = Vector3.zero;
		translateVertical = Vector3.zero;
	}
}
