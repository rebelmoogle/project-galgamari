﻿using UnityEngine;
using System.Collections;



public static class GameSettings {

	// just some settings. 
	// should not be changed from the outside, really. 
	// maybe later, if required. 

	// make only acessible in editor. 
	const float maxHeight = 50.0f;
	
	const float levelWidth = 20.0f;

	// write getter methods. 
	public static float MaxHeight {get {return maxHeight;}}
	public static float LevelWidth {get {return levelWidth;}}
}
