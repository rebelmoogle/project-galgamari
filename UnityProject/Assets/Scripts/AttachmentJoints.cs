﻿using UnityEngine;
using System.Collections;


// handles free and occupied connectors
// Connectortypes are stored in the tag as letters (each letter in the bone name after "TYPE" is a connector type, order does not matter)
// Components/Joints  with no type set through their name, default to Universal.
// no letters after Type or in the name = connects with any type.
// "ConnectorTypeA" = connects with Type A
// "BoneTypeB" = connects with Type B
// "BoneTypeAC" = connects with A or C
// etc. this allows the artists to define arbitrary connector types.
//
// To define a bone as a connector, it needs the word Connector in it.
// Bone names need to be unique per ship part. (example: UniqueNameConnectorTypeAB = a Connector Bone that connects to Type A or B)
public class AttachmentJoints : MonoBehaviour 
{

	Hashtable allAttachmentJoints = new Hashtable();
    Hashtable occupiedJoints = new Hashtable();
	Hashtable freeJoints = new Hashtable();
    /// <summary>
    /// How deep into the hierarchy should we search, starting from this? (up and down, distance from origin in number of parts)
    /// </summary>
    [Tooltip("How deep into the hierarchy should we search? (up and down, distance from origin in number of parts)")]
    public int maximumAttachSteps = 2;

    public float offsetDistance = 1.0f;

    /// <summary>
    /// Should Connectors be always recreated on Start?
    /// </summary>
    bool RecreateConnectorsOnStart = true;

    /// <summary>
    /// Can this gameObject collect others?
    /// </summary>
    [Tooltip("Can this gameObject collect others?")]
    public bool isCollector = false; //can we collect parts?

    /// <summary>
    /// Dirty = Collection power has to be updated for all children.
    /// If not collector, disconnect part?
    /// </summary>
    bool isDirty = false;

	// Use this for initialization
	void Start () 
	{
		// check all joints?
		// add a joint for each bone / socket defined for attachment in the mesh.

        //TODO: turn this into a Editor function; Call on import / attachment of component in Editor. 
        if(RecreateConnectorsOnStart) // we don't want to do this again, if we already have. 
        {
            Debug.Log("Recreating Connectors...");
            //register with attachment events
            // on attachment -> move joint to ConnectedJoints list
            // on detachment -> move joint back to freeJoints list. 
            Rigidbody partRigidBody = gameObject.GetComponent<Rigidbody>();
            if (partRigidBody == null)
            {
                Debug.LogWarning("NO RIGIDBODY SET, ADDING DEFAULT RIGIDBODY");
                partRigidBody = this.gameObject.AddComponent<Rigidbody>();
                partRigidBody.useGravity = false;  // no gravity in this bitch
            }

            Transform[] allTheBones = gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform currBone in allTheBones)
            {
                if (currBone.name.Contains("Connector"))
                {
                    int connectorTypeStrIndex = currBone.name.LastIndexOf("Type");
                    if (connectorTypeStrIndex < 0) // Type is not present
                    {
                        //add Type to the end.
                        currBone.name = currBone.name + "Type"; // index of the last Type should be followed by the Type Letters.
                    }
                    //newJoint.
                   // Vector3 temp = currBone.forward;
                   // currBone.localRotation.SetLookRotation(currBone.up, currBone.forward);
                    allAttachmentJoints.Add(currBone.name, currBone);
                    freeJoints.Add(currBone.name, currBone);
                    Debug.Log("Connector added: " + currBone.name);
                }

            }

            RecreateConnectorsOnStart = false;
        }

		
	}
	
	// Update is called once per frame
	void Update () 
	{
        //update all the dirty joints, if necessary.
	    if(isDirty)
        {
            foreach (FixedJoint curJoint in occupiedJoints.Values)
            {
                GameObject connectedPart = curJoint.connectedBody.gameObject;
                AttachmentJoints connectedPartJoints = connectedPart.GetComponent<AttachmentJoints>();
                if (connectedPartJoints != null)
                {
                    connectedPartJoints.isCollector = this.isCollector;
                    connectedPartJoints.isDirty = true;
                }
            }
            isDirty = false;
        }
	}

	// get first joint on this part that matches one of the connector types of the connectingpart
    // if this method stays internal / private: maybe just return the index. ;)
    //
    //Assumes that each connector on part is within maximumDistance.
    private string GetNearestFreeConnectorOnPart(GameObject partToConnect)
    {
        //Check my free connectors, return null if none found
        //Get the Connector Types as Char Array
        //TODO: use enum flags. (if we only need letters, that's always gonna be 26 anyway.
        // can convert the name to enum flags on start. (Each Connector has flags and part has flags for main connector to other ships / parts)
        char[] partConnectorTypes = partToConnect.name.Substring(partToConnect.name.LastIndexOf("Type") + "Type".Length).ToCharArray();
        string connectorTypes = "";

        // go through all connectors.
        // TODO: find connector closest to partToConnect
        foreach(Transform currConnector in freeJoints.Values)
        {
            connectorTypes = currConnector.name.Substring(currConnector.name.LastIndexOf("Type") + "Type".Length);
            // if the partConnectorTypes are empty it can connect to anything.
            // check for connector type
            if(partConnectorTypes.Length <= 0 || connectorTypes.Length <= 0 || connectorTypes.IndexOfAny(partConnectorTypes) >= 0)
            {
                // main connector and current connector have at least one connector type in common.
                // return the first one we find, no need to go through all.
                return currConnector.name;
            }
        }

        return null;
    }

    // attach a part
    /// <summary>
    /// Attaches a part to the specified connector
    /// </summary>
    /// <param name="freeJointName">Name of the connector in this object</param>
    /// <param name="partToConnect">part to attach to this connector</param>
    /// <returns>was the attachment successful?</returns>
    bool AttachAPart(string freeJointName, GameObject partToConnect)
    {
            //// We found a part! YAY!
            //// part cannot be attached without having a collectionhandler.
            //CollectionHandler partToConnectCollect = partToConnect.GetComponent<CollectionHandler>();
            //if (partToConnectCollect == null)
            //    return false;
        Debug.Log("Attaching new part to " + freeJointName);    

            // register with destruction
            DestructionHandler partToConnectDestruction = this.gameObject.GetComponent<DestructionHandler>();
            if (partToConnectDestruction != null)
            {
                partToConnectDestruction.DestructionImminentEvent += OnAttachedDestroy; // register with destruction event.
            }
            else
            {
                Debug.LogError("no destruction handler on part, attachment failed. Name:" + partToConnect.name + " ID: " + partToConnect.GetInstanceID());
                return false; //fail if no destruction event.
            }

            // Attach our part to the joint
            // remove joint from free joints 
            // (will be readded when a part is destroyed, register with attached part destroy event!)
            Transform foundBone = (Transform)freeJoints[freeJointName];
            FixedJoint newJoint = this.gameObject.AddComponent<FixedJoint>();
            newJoint.transform.parent = this.gameObject.transform;
            newJoint.connectedAnchor = foundBone.position;
            newJoint.transform.rotation = Quaternion.LookRotation(foundBone.up, foundBone.forward); // the joint shouldn't need extra rotation.

            //rotate and move the part to the connector
            partToConnect.transform.position = foundBone.position + foundBone.up * offsetDistance;
            partToConnect.transform.rotation = Quaternion.LookRotation(foundBone.up, foundBone.forward);
           

            newJoint.connectedBody = partToConnect.GetComponent<Rigidbody>(); // connect the two.
            //partToConnectCollect.ConnectedJointParent = this.gameObject; // set this as connected parent part. (the other part is connected to a joint of this one.)
            AttachmentJoints partToConnectJoints = partToConnect.GetComponent<AttachmentJoints>();
            if (partToConnectJoints != null)
            {
                partToConnectJoints.isCollector = this.isCollector; // new part becomes collector, if this is collector.
                partToConnectJoints.isDirty = true; // process all dirty parts as well.
            }
                

            freeJoints.Remove(freeJointName); // remove connector from free joints. (it is now occupied by the connected part)
            occupiedJoints.Add(freeJointName, newJoint);

            //inherit collector status from parent. 
            
            return true;
    }

    // First, check self for free connectors
    // If none found: ask attached parts for free connectors
    // If none found ask parent for free connectors
    //      - parent repeats at step 1, except for the child that asked.
    //      - should not ask parent if parent just asked.
    // BONUS: Each ship / pilot has a depth of search / maximum distance. (if parent or child to far from inital component, fail and return null)
    /// <summary>
    /// Finds a free joint withing set range and attaches given part to it.
    /// returns null if no connector found.
    /// </summary>
    /// <param name="partToConnect">The part to connect to the nearest free joint</param>
    /// <param name="partsVisited">How many steps have I taken through the hierarchy</param>
    /// <param name="askingPartID">The ID of the part that is asking for free connectors.</param>
    /// <returns>The part that we attached to (parent), null if none found</returns>
    public GameObject FindAndAttachPartToConnector(GameObject partToConnect, int partsVisited, int askingPartID)
    {
        Debug.Log("A new part is trying to connect, finding connector...");

        // this should never happen.
        // TODO ASSERT?
        if (askingPartID == this.gameObject.GetInstanceID())
            return null;

        // I cannot collect anything!
        if (!isCollector)
            return null;

        if (partsVisited >= maximumAttachSteps)
            return null; // too many steps, automatic fail.

        string freeJointName = GetNearestFreeConnectorOnPart(partToConnect);


        AttachmentJoints connectedPartJoints = partToConnect.GetComponent<AttachmentJoints>();
        if(freeJointName != null && AttachAPart(freeJointName, partToConnect))
        {
            return this.gameObject; // we are the parent.
        }
        
        // no free joints available or Attachment failed.
        // ask all attached parts?
        
        // ask children, exclude if ID == askingPartID
        GameObject foundPart = null;
        foreach(FixedJoint currOccupiedJoint in occupiedJoints.Values)
        {
            GameObject childPart = currOccupiedJoint.connectedBody.gameObject;
            if(childPart.GetInstanceID() == askingPartID)
            {
                continue;
            }

            AttachmentJoints childAttacher = childPart.GetComponent<AttachmentJoints>();
            if(childAttacher != null)
            {
                //make sure it doesn't go back up or down the tree
                foundPart = childAttacher.FindAndAttachPartToConnector(partToConnect, partsVisited - 1, this.gameObject.GetInstanceID());
                if (foundPart != null) // recursiveness happens here, make sure this doesn't go endless.
                {
                    return foundPart; // found something and connected! return the part!
                }
                else
                    continue; // did not find anything, check next one
            }
        }

        // No Connectors in children, ask parent, except if id == askingPartid
        // check if this is a collectible part, otherwise it does not have a parent. (Core)
        CollectionHandler thisCollector = this.GetComponent<CollectionHandler>();
        if(thisCollector != null)
        {
            // we are collectible, so we might have a parent!
            // make sure parent exists and is not the one originally asking. 
            if (thisCollector.ConnectedJointParent != null && thisCollector.ConnectedJointParent.GetInstanceID() != askingPartID)
            {
                AttachmentJoints parentJoints = thisCollector.ConnectedJointParent.GetComponent<AttachmentJoints>();
                if(parentJoints != null) // only check for connectors if parent has any.
                    return parentJoints.FindAndAttachPartToConnector(partToConnect, partsVisited - 1, this.GetInstanceID());
            }
        }
        

        // found none. Attachment failed
        return null;
    }

    // free up joint and move back to free joints when connected part destroyed.
    void OnAttachedDestroy(GameObject partThatWasDestroyed)
    {
        FixedJoint disconnectJoint  = (FixedJoint)occupiedJoints[partThatWasDestroyed.name];
        //disconnectJoint.connectedBody = null; // part will be destroyed.
        freeJoints.Add(partThatWasDestroyed.name, allAttachmentJoints[partThatWasDestroyed.name]); // put back in Freejoints.
        occupiedJoints.Remove(partThatWasDestroyed.name);
    }

    void OnDestroy()
    {
        // TODO: children have to register with Destruction event of this?
        // remove ourselves from our childen, then destroy each joint. 
        foreach(FixedJoint curJoint in occupiedJoints.Values)
        {
            if (curJoint.connectedBody == null) // no attached body. or body already destroyed.
                continue;

            GameObject connectedPart = curJoint.connectedBody.gameObject;
            CollectionHandler connectedCollect = connectedPart.GetComponent<CollectionHandler>();
            if (connectedCollect != null) // TODO THIS SHOULD NEVER HAPPEN. ASSERT?
                connectedCollect.ConnectedJointParent = null;

            AttachmentJoints connectedPartJoints = connectedPart.GetComponent<AttachmentJoints>();
            if(connectedPartJoints != null)
            {
                connectedPartJoints.isCollector = false;
                connectedPartJoints.isDirty = true;
                // #### TODO: don't all of it's children have to be updated too? ####
                // mark as dirty, update in update if dirty?
            }


        }

        allAttachmentJoints.Clear();
        freeJoints.Clear();
        occupiedJoints.Clear();
        //TODO: check if clear actually destroys created joints.
        // if not the Destruction of the object should destroy all attached components anyway.
        
    }

}
