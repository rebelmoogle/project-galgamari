﻿using UnityEngine;
using System.Collections;

public class BlastOffPart : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void BlastIntoSpace(Vector2 splosionParameters)
	{
		if(this.transform.parent == null)
		{
			return;
			//already floating in space
		}


		Vector3 parentPosition = this.transform.root.position;
		this.transform.parent = null; // time to leave home and see the world. Or space, respectively

		if(this.GetComponent<Rigidbody>() != null)
		{
			this.GetComponent<Rigidbody>().isKinematic = false;
			this.GetComponent<Rigidbody>().AddExplosionForce(splosionParameters.x, parentPosition, splosionParameters.y);
		}
	}

}
