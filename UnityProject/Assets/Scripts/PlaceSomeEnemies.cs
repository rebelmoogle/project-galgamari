﻿using UnityEngine;
using System.Collections;

public class PlaceSomeEnemies : MonoBehaviour 
{

	//get the city build so we can easily see where buildings are?
	// probably rather do a raycast.

	public GameObject enemyPrefab;
	// set of enemies? 
	// randomize?
	// give enemies different properties? (size, firepower, attached elements, looks, etc)
	public float forwardDistance = 20.0f;
	public float distanceDeviation = 0.2f;

	public float chanceOfSpawningPerSecond = 0.5f;
	float elapsedTimeSinceSpawn;

	Vector2 widthHeight;

	Camera mainCamera;

	int allButPlaneLayer;

	// Use this for initialization
	void Start () 
	{
		// make sure enemy is properly set
		if(enemyPrefab == null)
			Debug.LogError("Enemy prefab not set in EnemyPlacer");	// otherwise create cube or trow error

		// only spawn once time has elapsed at least a second, or set interval.
		elapsedTimeSinceSpawn = 0.0f;

		mainCamera = Camera.main;

		// get maximum height and width from game settings.
		widthHeight = new Vector2(GameSettings.MaxHeight, GameSettings.LevelWidth);

		allButPlaneLayer = 1<<8; // layer 8
		allButPlaneLayer = ~allButPlaneLayer; //inverse of that, so we get all but layer 8
	}
	
	// Update is called once per frame
	void Update () 
	{
		//spawndatshit
		elapsedTimeSinceSpawn += Time.deltaTime;
		// check if time since last spawn is more than a second
		if(elapsedTimeSinceSpawn >= 1.0f) // throw some dice and see if we get lucky. 
		{
			elapsedTimeSinceSpawn = 0.0f; // only try once a second.
			if(Random.value <= chanceOfSpawningPerSecond)
			{
				// calculate semi-random position, in front of player.
				Vector3 enemyPosition = new Vector3(	Random.Range(- widthHeight.x/2, widthHeight.x/2), 
				                                    widthHeight.y, 
				                                    mainCamera.transform.position.z + forwardDistance + (forwardDistance * Random.Range(-distanceDeviation, distanceDeviation)));
				// camera + forward + forwarddeviation percentage
				
				RaycastHit whatsDownThere;
				if(Physics.Raycast(enemyPosition, Vector3.down, out whatsDownThere, widthHeight.y * 2 ,allButPlaneLayer))
				{
					// we hit something! EVASIVE MANEAUVERS!
					float objectHeight = whatsDownThere.collider.transform.root.transform.localScale.y; // should be a unit cube.... otherwise we'll be in trouble, haha.
					// TODO: alternatively: check if completely risen, then check distance. 
					// OR: just rise enemys with buildings
					// OR: have enemys navigate by themselves, heh. AI AND STUFF
					
					//check if object low enough
					if(objectHeight < widthHeight.y/2)
					{
						// -> recalc height (random between object height and maxheight
						enemyPosition.y = Random.Range(objectHeight, widthHeight.y);
						
						// spawn & reset elapsedtim
						SpawnEnemy(enemyPosition);
						
					}
				}
				else
				{
					// nothing? really? this position is good to spawn!
					
					// -> recalc height (random between ground and maxheight
					enemyPosition.y = Random.Range(10.0f, widthHeight.y);
					
					// spawn & reset elapsedtime
					SpawnEnemy(enemyPosition);
				}
			}

		}
	}

	void SpawnEnemy(Vector3 enemyPosition)
	{
		// just spawn dat shit.
		GameObject enemyClone;
		enemyClone = (GameObject)Instantiate(enemyPrefab, enemyPosition, Quaternion.identity);
	}
}
