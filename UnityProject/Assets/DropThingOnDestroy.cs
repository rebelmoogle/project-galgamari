﻿using UnityEngine;
using System.Collections;

public class DropThingOnDestroy : MonoBehaviour {

	public GameObject thingToDrop;

	public float dropChance = 0.5f;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	void OnDestroy()
	{
		if(thingToDrop != null && Random.value < dropChance) //&& lazors.rigidbody != null)
		{
			GameObject thingClone = (GameObject)Instantiate(thingToDrop, this.transform.position, this.gameObject.transform.rotation);
			thingClone.GetComponent<Rigidbody>().isKinematic = false;
			//thingClone.rigidbody.AddExplosionForce(50, this.transform.position, 5);
			//TODO: couple strength with size//effects and damage!
			
		}
	}
}
