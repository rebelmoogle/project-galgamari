﻿using UnityEngine;
using System.Collections;

public class AsteroidGenerator : MonoBehaviour {

	public GameObject asteroid;
	public float asteroidsPerSecond = 1.0f;
	public Camera mainCamera;

	public float maxWidth;
	public float maxHeight;
	[Range(1,500)]
	public float creationDistance;
	float currentWidth;
	float currentHeight;

	float currentTimer = 0.0f;
	// Use this for initialization
	void Start () 
	{
		currentTimer = 1.0f / asteroidsPerSecond;
		currentWidth = maxWidth;
		currentHeight = maxHeight;

		if(Camera.main)
		{
			mainCamera = Camera.main;
		}
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//we assume the starfighter is always moving forward.

		// generate a new asteroid just outside the view frustum, every few seconds.
		if(currentTimer > 0.0f)
			currentTimer -= Time.deltaTime;
		else
		{
			currentTimer = 1.0f / asteroidsPerSecond;
			if(currentWidth > 20)
				currentWidth -= currentTimer;

			if(currentHeight > 10)
				currentHeight -= currentTimer;

			// generate asteroid just outside frustrum, at random width and height
			GameObject asteroidClone;
			asteroidClone = (GameObject)Instantiate(asteroid, GetRandomPosition(mainCamera.transform.position + Vector3.forward * creationDistance, maxWidth, maxHeight), mainCamera.transform.rotation);
			asteroidClone.transform.localScale = Vector3.one * Random.Range(1.0f, 2.0f);
			asteroidClone.GetComponent<Rigidbody>().AddTorque(Random.insideUnitSphere * 10, ForceMode.Impulse);
			// give random force and torque



		}

	}

	Vector3 GetRandomPosition(Vector3 centerPosition, float width, float height)
	{
		Vector3 randomPosition = Vector3.zero;
		// x
		randomPosition.x = Random.Range(-width/2, width/2);
		// y
		randomPosition.y = Random.Range(-height/2, height/2);

		return centerPosition + randomPosition;
	}
}
