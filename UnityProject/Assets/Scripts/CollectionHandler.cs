﻿using UnityEngine;
using System.Collections;

/// <summary>
/// CollectionHandler: handles being collected by another component?
/// Power to collect on AttachmentJoints?
/// </summary>
public class CollectionHandler : MonoBehaviour {

	public delegate void EventHandler(GameObject e);
	public event EventHandler CollectedByPlayer;

	//public event EventHandler ShotIntoSpace;

    // TODO: INTEGRATE WITH NEW ATTACHMENTJOINT SYSTEM

    public bool isCollectible = true;

    // what part are we connected to? // who is our parent?
    GameObject connectedToPart = null;
    public GameObject ConnectedJointParent
    {
        get { return connectedToPart; }
        set 
        { 
            //TODO check if actually connected.
            if(value != null)
                connectedToPart = value; 
        }
    }

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnCollisionEnter(Collision collision)
	{
        if (connectedToPart != null || !isCollectible)
            return; // we are already connected. // no further processing. 

        Debug.Log("We collided with something, finding connectors");
        // if this is collectible (isn't that signified by having this class?)
        // AND Collider has ATTACHMENTJOINTS, we can connect. 
        // else fail.
        // TODO: Make a switch for everything being able to collect. (game mode / state / settings)
        AttachmentJoints colliderAttachJoints = collision.collider.transform.root.GetComponent<AttachmentJoints>();
        if (isCollectible && colliderAttachJoints != null)
        {
            Debug.Log("Collider has connectors, trying to connect...");
            // allright, we have a collider with joints to attach to, let's check if there are free ones.
            // call AttachmenJoints on collider (if it has one), pass this.gameobject.
            // make sure it has a rigidbody? -> already checked by attachmenjoints on start
            connectedToPart = colliderAttachJoints.FindAndAttachPartToConnector(this.gameObject, 0, this.GetInstanceID());
        }

        #region old deprecated attachment code
        ///// FALLBACK: NO ATTACHMENTJOINTS ON HIT OBJECT. (If it doesn't have one, shouldn't we just not connect?
        ///// OR: Move one higher if we find no ATTACHMENTJOINTS
        //if(this.transform.parent != null)
        //    return; //ignore collision, we already have a parent!

        //if (collision.collider.transform.root.tag == "Player" && isCollectible) 
        //{
        //    Transform colliderParent = collision.collider.transform.root; // we want the topmost parent of the thing we collided with.

        //    // Yay a parent we can cling to!
        //    this.transform.parent = colliderParent;
        //    // snap to mesh along normal
        //    this.transform.rotation = Quaternion.LookRotation(collision.contacts[0].normal);
        //    this.transform.position += this.transform.root.forward;
        //    // get normal
        //    // rotate towards normal
        //    // calc distance from parent mesh
        //    // move slightly away from mesh to prevent collision?

        //    if(this.rigidbody != null )
        //    {
        //        this.rigidbody.isKinematic = true;
        //    }

        //    // trigger collection event so that other methods / components can react (enable fire handling, for example)
        //    OnCollectedByPlayer(colliderParent.gameObject);

        //}
        #endregion
    }

    /// <summary>
    /// DEPRECATED: REPLACED BY OnAttach in Attachmentjoints
    /// </summary>
    /// <param name="player">the collecting player.</param>
	void OnCollectedByPlayer(GameObject player)
	{
		if(CollectedByPlayer != null)
			CollectedByPlayer(player); // give other methods a chance to connect to the players input methods.
	}
}
