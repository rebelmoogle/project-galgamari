﻿using UnityEngine;
using System.Collections;

public class CityBlockGenerator : MonoBehaviour {
	
	// TODO: RANGES
	public float cityWidth = 700.0f;

	public float blockWidth = 5.0f;
	public float deviation = 0.1f; // this one is percentage

	public float blockHeight = 15.0f;
	public float heightDeviation = 0.5f; // this one is percentage

	public float streetWidth = 1.0f;
	public float streetDeviation = 0.2f;

	public float alleyWidth = 0.5f;
	public float alleyDeviation = 0.1f;

	public int maxBuildings = 100;

	public float startDistance = 50.0f;

	// building prefab
	public GameObject buildingPrefab; // should be just a 1m³ cube // should automatically rise up and also destroy itself

	Vector2 currentPosition = Vector2.zero;

	public Vector2 CurrentPosition {get {return currentPosition;}}

	// Use this for initialization
	void Start () 
	{
	
		if(buildingPrefab == null)// init check prefab is there, otherwise create cube
		{
			buildingPrefab = GameObject.CreatePrimitive(PrimitiveType.Cube);
			//attach scripts
		}

		currentPosition.x = -cityWidth/2 + (alleyWidth + Random.Range(-alleyDeviation, alleyDeviation)) + blockWidth/2;
		currentPosition.y += startDistance;
		// start with buildings, not a street.

		// set of prefabs? // different building types?
		// set of addons? randomly attach to buildings?

	}
	
	// Update is called once per frame
	void Update () 
	{
		//number of buildings currently present.
		if(transform.childCount < maxBuildings)
		{
			float blockScaling = Random.Range(-deviation, deviation);
			//currentPosition.x += blockScaling/2; // alley acts as buffer for now.
			CreateShit(currentPosition, blockScaling);

			if(currentPosition.x + blockWidth + alleyWidth >= Camera.main.transform.position.x + cityWidth / 2)
			{
				// END OF THE LINE! EVERYONE OUT DAMN YOU!
				// back to square one
				currentPosition.x = Camera.main.transform.position.x -cityWidth/2 + (alleyWidth + alleyWidth * Random.Range(-alleyDeviation, alleyDeviation)) + blockWidth/2;
				//move on up.
				currentPosition.y = currentPosition.y + streetWidth + streetWidth * Random.Range(-streetDeviation, streetDeviation) + blockWidth + deviation;
			}
			else
			{
				// we have enough space left, let's calculate the next position
				currentPosition.x = currentPosition.x + blockWidth + (alleyWidth + alleyWidth * Random.Range(-alleyDeviation, alleyDeviation));
			}
		}
	}

	void CreateShit(Vector3 placementPosition, float scalingFactor)
	{
		GameObject buildingClone;

		buildingClone = (GameObject)Instantiate(buildingPrefab, new Vector3(placementPosition.x, 0, placementPosition.y), Quaternion.identity);
		buildingClone.transform.localScale = new Vector3(	blockWidth + blockWidth * scalingFactor, 
		                                                 	blockHeight + blockHeight * Random.Range(-heightDeviation, heightDeviation), 
		                                                 	blockWidth + blockWidth * scalingFactor);
		//place buildings below ground, so they can rise up.
		buildingClone.transform.position = buildingClone.transform.position - new Vector3(0, buildingClone.transform.localScale.y, 0); // center is at bottom
		buildingClone.transform.parent = this.transform; // it is our child after all.
		buildingClone.GetComponentInChildren<Renderer>().material.color = new Color(Random.value*0.7f, Random.value*0.7f, Random.value*0.7f);
		
	}

	// method generate building
	// method generate addons. 
	// add some lights?
	//TODO buildings rise out of the ground, awesomeness, yeah!
	// the buildings rise themselves. muhahaha
}
